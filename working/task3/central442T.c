#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define NUM_PROCESSES 4			// Number of external processes
#define MAX_THREADS 2			// Number of threads
#define THREAD_1 0				// Thread 1 id
#define THREAD_2 1				// Thread 2 id
#define MESSAGE_HEADER_SIZE 30

// Function Declaration/prototype
void * stable_temp(void* args);

struct mailbox {
	long priority;	// Message priority
	int temp;		// Temperature
	int pid;		// Process id
	int stable;		// Boolean for temperature stability
};

// Setting up args for each thread
struct thread_args {
	int temp;							// Temp
	int mailbox_id;						// ID
	char header[MESSAGE_HEADER_SIZE];	// Message header, displays on print
};

// Main
int main(int argc, char *argv[]) {

	int returnStatus = 0;
	pthread_t * central_threads[MAX_THREADS];
	struct thread_args * thread_args1 = NULL;
	struct thread_args * thread_args2 = NULL;

	// Check for enough args
	if(argc != 5) {
		printf("USAGE: Too few arguments --./central.out Temp");
		exit(0);
	}

	// Setup the thread_args structs
	// First we allocate our memory
	thread_args1 = malloc(sizeof(struct thread_args));
	thread_args2 = malloc(sizeof(struct thread_args));
	central_threads[THREAD_1] = malloc(sizeof (pthread_t));
	central_threads[THREAD_2] = malloc(sizeof (pthread_t));

	// Set up our variables
	// Thread 1
	thread_args1->temp = atoi(argv[1]);
	thread_args1->mailbox_id = atoi(argv[3]);
	strcpy(thread_args1->header, "[352959400]");
	// Thread 2
	thread_args2->temp = atoi(argv[2]);
	thread_args2->mailbox_id = atoi(argv[4]);
	strcpy(thread_args2->header, "[352959411]");

	printf("\nStarting Server...\n");

	// Create the two threads
	// main() will control the threads, stable_temp takes the duty that main
	// had in the older version of "central.c"

	// Firstly, 1-4)
	returnStatus = pthread_create(central_threads[THREAD_1], NULL, stable_temp, (void *) thread_args1);
	if(returnStatus != 0) {
		fprintf(stderr, "!! Thread_1 failed\n");
		free(thread_args1);
		free(thread_args2);
		free(central_threads[THREAD_1]);
		free(central_threads[THREAD_2]);
		exit(0);
	}

	// Secondly, 5-8
	returnStatus = pthread_create(central_threads[THREAD_2], NULL, stable_temp, (void *) thread_args2);
	if(returnStatus != 0) {
		fprintf(stderr, "!! Thread_2 failed\n");
		free(thread_args1);
		free(thread_args2);
		free(central_threads[THREAD_1]);
		free(central_threads[THREAD_2]);
		exit(0);
	}

	// Both parts need to be finished
	pthread_join(*central_threads[THREAD_1], NULL);
	pthread_join(*central_threads[THREAD_2], NULL);
	
	// Prevent memory problems, please
	free(thread_args1);
	free(thread_args2);
	free(central_threads[THREAD_1]);
	free(central_threads[THREAD_2]);

	printf("Exiting...\n");

	return 0;
}

// Essentially our peice from the other tasks
// But we'll run it in two threads this time.
void * stable_temp(void * args) {
	struct timeval t1, t2;
	double elapsedTime;
	struct mailbox msgp;	// These two have to be here
	struct mailbox cmbox;	// Otherwise the info is shared incorrectly

	// Start timer
	gettimeofday(&t1, NULL);

	// Convert void pointer
	struct thread_args * arguments = (struct thread_args *) args;

	// Set up local variables
	int i,result,length,status = 0;	// Counter for loops
	int uid = 0;					// Central PID
	int initTemp = 0;				// Initial temp
	int msqid[NUM_PROCESSES];		// Mailbox ID
	int unstable = 1;				// Bool for stable threads
	int tempAry[NUM_PROCESSES];
	int CENTRAL_MAILBOX = 3529594;
	char process_name[50] = {0};	// Process name (varies per thread)

	// Setup our local variables
	initTemp = arguments->temp;
	CENTRAL_MAILBOX = arguments->mailbox_id;
	strcpy(process_name, arguments->header);

	// Create central mailbox
	int msqidC = msgget(CENTRAL_MAILBOX, 0600 | IPC_CREAT);

	// Create our external mailboxes
	for(i = 1; i <= NUM_PROCESSES; i++){
		msqid[(i-1)] = msgget((CENTRAL_MAILBOX + i), 0600 | IPC_CREAT);
	}

	// Initialize our message
	msgp.priority = 1;
	msgp.pid = uid;
	msgp.temp = initTemp;
	msgp.stable = 1;

	// Set the length of our message
	length = sizeof(msgp) - sizeof(long);
	
	// This is our main loop, keeps running so long as things are unstable
	while(unstable == 1){
		int total = 0;	// Sum up the temps as we loop
		int stable = 1;	//stability trap

		// Check external processes
		for(i = 0; i < NUM_PROCESSES; i++) {
			result = msgrcv( msqidC, &cmbox, length, 1, 0);

			// Check for stability
			if(tempAry[(cmbox.pid - 1)] != cmbox.temp) {
				stable = 0;
				tempAry[(cmbox.pid - 1)] = cmbox.temp;
			}

			// Add up temps as we go
			total += cmbox.temp;
		}

		/*When all the processes have the same temp twice: 1) Break the loop 2) Set the messages stable field to stable*/
		if(stable) {
			printf("%s Temperature Stabilized: %d\n", process_name, msgp.temp);
			unstable = 0;
			msgp.stable = 0;
		}
		else { //Calculate a new temp and set the temp field in the message
			int newTemp = (msgp.temp + 1000*total) / (1000*NUM_PROCESSES + 1);
			usleep(100000);
			msgp.temp = newTemp;
			printf("%s The new temp in central is %d\n",process_name, newTemp);
		}

		/* Send a new message to all processes to inform of new temp or stability */
		for(i = 0; i < NUM_PROCESSES; i++) {
			result = msgsnd( msqid[i], &msgp, length, 0);
		}
	}

	gettimeofday(&t2, NULL);

	printf("\n%s Shutting down thread...\n", process_name);

	// Remove the mailbox
	status = msgctl(msqidC, IPC_RMID, 0);

	// Validate nothing went wrong when trying to remove mailbox
	if(status != 0) {
		printf("\nERROR closing mailbox\n");
	}


	// Compute and print the elapsed time in millisec
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;		// sec to ms
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;	// us to ms
	printf("%s The elapsed time is %fms\n",process_name, elapsedTime);
	// Tell main that the thread has finished
	pthread_exit(NULL);
}