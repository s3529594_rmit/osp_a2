#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct {
	long priority;	//message priority
	int temp;		//temperature
	int pid;		//process id
	int stable;		//boolean for temperature stability
} msgp, cmbox;

int main(int argc, char *argv[]) {

	// Check for right amount of arguments
	if(argc != 4) {
		printf("USAGE: Too few arguments --./central.out Temp UID");
		exit(0);
	}

	// Setup local variables
	int unstable = 1;
	int result, length, status;
	int initTemp = atoi(argv[1]);
	int uid = atoi(argv[2]);
	int CENTRAL_MAILBOX = atoi(argv[3]);

	// Check what processes we're dealing with
	if(uid > 4) {
		uid = (uid - 4);
	}
	// Create the Central Servers Mailbox
	int msqidC = msgget(CENTRAL_MAILBOX, 0600 | IPC_CREAT);

	// Create the mailbox for this process and store it's IDs
	int msqid = msgget((CENTRAL_MAILBOX + uid), 0600 | IPC_CREAT);

	// Initialize the message to be sent
	cmbox.priority = 1;
	cmbox.pid = uid;
	cmbox.temp = initTemp;
	cmbox.stable = 1;

	// Length of the messages
	length = sizeof(msgp) - sizeof(long);

	// While we do not have stability
	while(unstable == 1) {
		// Send
		result = msgsnd( msqidC, &cmbox, length, 0);

		// Wait
		result = msgrcv( msqid, &msgp, length, 1, 0);

		// If we're stable then we can move on
		if(msgp.stable == 0) {
			unstable = 0;
			printf("\nProcess %d Temp: %d\n", cmbox.pid, cmbox.temp);
		}
		else { // Otherwise new temp
			int newTemp = (10*cmbox.temp + msgp.temp) / 11;
			cmbox.temp = newTemp;
		}
	}

	// Remove the mailbox
	status = msgctl(msqid, IPC_RMID, 0);

	// Check nothing when wrong when trying to remove mailbox
	if(status != 0) {
		printf("\nERROR closing mailbox\n");
	}

	return 0;
}
