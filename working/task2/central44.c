#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Two sets of 4, really. But we'll be using fork()
#define NUM_PROCESSES 4		// Total number of external processes

struct {
	long priority;	// Message priority
	int temp;				// Temperature
	int pid;				// Process id
	int stable;			// Boolean for temperature stability
} msgp, cmbox;

// Main
int main(int argc, char *argv[]) {
struct timeval t1, t2;
		double elapsedTime;

		// Start timer
		gettimeofday(&t1, NULL);

		// Got to make sure we have the right amount of arguments
		if(argc != 5) {
			printf("USAGE: Too few arguments --./central.out Temp");
			exit(0);
		}

		printf("\nStarting Server...\n");

		// Set up local variables
		int i,result,length,status;			// Counters
		int uid = 0;										// Central process ID
		int initTemp = 0;								// Starting temperature
		int msqid[NUM_PROCESSES];				// Mailbox IDs for all processes
		int unstable = 1;								// Boolean to denote temp stability
		int tempAry[NUM_PROCESSES];			// Array of process temperatures
		int pid = 0;										// For use with fork()
		int CENTRAL_MAILBOX = 3529594;	// Necessary

		// Setup our two-pronged attack
		pid = fork();

		// Check outcome of fork()
		if(pid < 0) {
			printf("There was an error with forking!");
		}
		else if(pid == 0) {
			initTemp = atoi(argv[2]);
			CENTRAL_MAILBOX = atoi(argv[4]);
		}
		else {
			initTemp = atoi(argv[1]);
			CENTRAL_MAILBOX = atoi(argv[3]);	
		}

		// Create the Central Servers Mailbox
		int msqidC = msgget(CENTRAL_MAILBOX, 0600 | IPC_CREAT);

		// Create the mailboxes for external processes and store their IDs
		for(i = 1; i <= NUM_PROCESSES; i++) {
			msqid[(i-1)] = msgget((CENTRAL_MAILBOX + i), 0600 | IPC_CREAT);
		}

		// Initialize our message to be sent
		msgp.priority = 1;
		msgp.pid = uid;
		msgp.temp = initTemp;
		msgp.stable = 1;

		/* The length is essentially the size of the structure minus sizeof(mtype) */
		length = sizeof(msgp) - sizeof(long);

		// While the processes have different temperatures
		while(unstable == 1) {
		int sumTemp = 0;				// Sum up the temps as we loop
		int stable = 1;					// Stability bool

		// Get new messages from the processes
		for(i = 0; i < NUM_PROCESSES; i++) {
				result = msgrcv( msqidC, &cmbox, length, 1, 0);

			/* If any of the new temps are different from the old temps then we are still unstable. Set the new temp to the corresponding process ID in the array */
			if(tempAry[(cmbox.pid - 1)] != cmbox.temp) {
				stable = 0;
				tempAry[(cmbox.pid - 1)] = cmbox.temp;
			}

			// Add up all the temps as we go for the temperature algorithm
			sumTemp += cmbox.temp;
		}

		/*When all the processes have the same temp twice: 1) Break the loop 2) Set the messages stable field to stable*/
		if(stable) {
			printf("Temperature Stabilized: %d\n", msgp.temp);
			unstable = 0;
			msgp.stable = 0;
		}
		else { //Calculate a new temp and set the temp field in the message
			int newTemp = (msgp.temp + 1000*sumTemp) / (1000*NUM_PROCESSES + 1);
			usleep(100000);
			msgp.temp = newTemp;
			printf("The new temp in central is %d\n",newTemp);
		}

		// Send a new message to all processes to inform of new temp or stability
		for(i = 0; i < NUM_PROCESSES; i++) {
			result = msgsnd( msqid[i], &msgp, length, 0);
		}
	} // End While

	// stop timer
	gettimeofday(&t2, NULL);

	// Race Condition stuff
	if(pid > 0) {
		printf("Waiting for Child process...\n");
		wait(&status); // Wait for the kids to come home
	}

	printf("\nShutting down Server...\n");

	// Remove the mailbox
	status = msgctl(msqidC, IPC_RMID, 0);

	// Validate nothing when wrong when trying to remove mailbox
	if(status != 0) {
		printf("\nERROR closing mailbox\n");
	}

	// Compute and print the elapsed time in millisec
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;			// sec to ms
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;	// us to ms
	printf("The elapsed time is %fms\n", elapsedTime);

	// Escape
	return 0;
}
